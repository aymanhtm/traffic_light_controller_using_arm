/*      Computer Architectures - 02LSEOV 02LSEOQ - Extrapoint_01				
-----------------------------------------------------------------------    
Author:							Ayman HATOUM - 263893 - Politecnico di Torino           
File name:          main_program.c
Descriptions:       main function of the program
Creation:						03 January 2019									    
Last Update:  			03 January 2019						               
-----------------------------------------------------------------------	*/								

#include <stdio.h>
#include "LPC17xx.h"
#include "led/led.h"
#include "button/button.h"
#include "timer/timer.h"

int main (void) {
  	
	SystemInit();  												//System Initialization (i.e., PLL)  
  LED_init();                           //LED Initialization                 
  BUTTON_init();												//BUTTON Initialization              
			
	init_timer(0,0x165A0BC0,0x1DCD6500);							//TIMER0 Initialization for 15s TimerInterval0 and 20s TimerInterval1             
	init_timer(1,0x00BEBC20,0x07735940);							//TIMER1 Initialization for 0.5s TimerInterval0 and 5s TimerInterval1 
	enable_timer(0);
	
	LPC_SC->PCON |= 0x1;									//power-down	mode										
	LPC_SC->PCON &= 0xFFFFFFFFD;						
		
  while (1) {                           //Loop forever and wait for interrupt                      	
		__ASM("wfi");
  }
}