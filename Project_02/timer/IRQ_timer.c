/*      Computer Architectures - 02LSEOV 02LSEOQ - Extrapoint_02				
-----------------------------------------------------------------------    
Author:							Ayman HATOUM - 263893 - Politecnico di Torino           
File name:          IRQ_timer.c
Descriptions:       functions to manage T0, T1 & T2 interrupts
Creation:						10 January 2019									    
Last Update:  			17 January 2019						               
-----------------------------------------------------------------------	*/				
		
#include "lpc17xx.h"
#include "timer.h"
#include "../led/led.h"
#include "../RIT/RIT.h"
#include "../ADC/adc.h"

int state = 0, status = 1, i = 0, conf, t = 0; 
extern int maint, int0_pressed;
extern float volume;

uint16_t SinTable[45] =     			//45 five samlples between 0 & 1023 to ensure the highest amplitude wave in the case of 10 bit DAC 								                                  
{
    512, 583, 653, 719, 783, 841, 892, 936, 972,
    998, 1015, 1023, 1020, 1008, 985, 954, 914, 867, 
    812, 752, 687, 618, 547, 476, 405, 337, 271,
    211, 156, 109, 69, 37, 15, 2, 0, 7, 25, 51, 87,
		131, 182, 241, 303, 371, 441
};

void TIMER0_IRQHandler (void)
{	
	if(state == 0)
	{
		disable_timer(1);
		disable_timer(2);
		reset_timer(1);
		reset_timer(2);
		LPC_TIM1->MCR = 3;						//activate reset and interrupt for MR0 of TIMER1 which count 0.5s
		enable_timer(1);
		enable_timer(2);
		conf = 0;											//avoids the "conf" execution in TIMER1_IRQHandler
	}
	else
	{
		status++;											//used in the switch statement of TIMER1_IRQHandler
		disable_timer(1);
		reset_timer(1);
		disable_timer(2);
		reset_timer(2);
		LPC_SC->PCONP &= ~(1 << 22);	//cutting power from TIMER2 to save power since its not used for now
		LED_Out(0x88);								//turn ON LEDs 4 and 8 (red pedestrian and green car)
	}
	state++;												//next state
	LPC_TIM0->IR = 2;								//clear interrupt flag of MR1
	LPC_TIM0->IR = 1;								//clear interrupt flag of MR0 
	return;
}


void TIMER1_IRQHandler (void)
{
	
	
	if(maint == 1)									//Maint State only executed
	{
		static int k = 0;
		if(k%2 == 0)
			{
				disable_timer(2);					//beeping OFF
				reset_timer(2);
				LED_Out(0x0);							//all lights are OFF
			}
			else
			{
				enable_timer(2);					//beeping ON
				LED_Out(0x90);						//turn ON LEDs 4 and 7 (red pedestrian and yellow car)
			}
			k++;												//keeps flashing for 1 s intervals
	}
	else
	{
		if(conf == 1)									//executed when in State 0
		{
			if(t%2 == 0)
			{
				if(int0_pressed == 0)
				{
				disable_timer(2);					//beeping OFF
				reset_timer(2);
				}
			}
			else
				enable_timer(2);					//beeping ON
			t++;												//ON/OFF for intervals of 1 s
		}
		else
		{
		switch(status)							//from state 1 and above
		{
			case 1:
				if(i%2==0)
				{
					LED_Off(6);						//flashing OFF of LED 5 (green pedestrian)
					if(int0_pressed == 0)	//for ensuring the continues beeping of INT0 pressing
					{
					disable_timer(2);			//beeping OFF
					reset_timer(2);
					}
				}
				else
				{
					LED_On(6);						//flashing ON of LED 5 (green pedestrian)
					enable_timer(2);			//beeping ON
				}
				i++;										//ON/OFF for intervals of 0.5 s
				break;
			case 2:
				if(LPC_TIM1->IR == 2)		//when debugging it was noticed that TIMER1 interrupt flag for previous count (0.5s) was stuck,
				{												//so to make sure only to enter if it comes from the count of MR1 which is 5s
				LED_Out(0x90);					//turn ON LEDs 4 and 7 (red pedestrian and yellow car)
				state++;								//nest state
				status++;
				}
				break;
			case 3:
				state = 0;				
				LED_Out(0x60);					//turn ON LEDS 5 and 6 (green pedestrian and red car)
				disable_timer(1);
				reset_timer(1);
				init_timer(1,0x00BEBC20,0x017D7840);		//TIMER1 Initialization for 0.5s TimerInterval0 and 1s TimerInterval1 
				enable_timer(0);
				enable_timer(1);
				LPC_SC->PCONP |= (1 << 22);							//Activation of power for TIMER2
				enable_timer(2);
				status = 1;
				conf = 1;
				break;
			default:
				break;
		}
		}
	}
	LPC_TIM1->IR = 2;						//clear interrupt flag of MR1	
  LPC_TIM1->IR = 1;			      //clear interrupt flag of MR0
  return;
}

void TIMER2_IRQHandler (void)
{
	static int ticks=0;
	/* DAC management */
	LPC_DAC->DACR = (int)((float)volume*SinTable[ticks])<<6;	//output multiplied by volume factor which is initialized ar 5
	ticks++;
	if(ticks==45) ticks=0;
	
	LPC_TIM2->IR = 2;						//clear interrupt flag of MR1	
  LPC_TIM2->IR = 1;			      //clear interrupt flag of MR0
  return;
}