/*      Computer Architectures - 02LSEOV 02LSEOQ - Extrapoint_02				
-----------------------------------------------------------------------    
Author:							Ayman HATOUM - 263893 - Politecnico di Torino           
File name:          main_program.c
Descriptions:       main function of the program
Creation:						10 January 2019									    
Last Update:  			17 January 2019						               
-----------------------------------------------------------------------	*/								

#include <stdio.h>
#include "LPC17xx.h"
#include "led/led.h"
#include "button/button.h"
#include "timer/timer.h"
#include "RIT/RIT.h"
#include "joystick/joystick.h"
#include "ADC/adc.h"
#include "DAC/DAC.h"

extern int conf;

int main (void) {
  	
	SystemInit();  												//System Initialization (i.e., PLL)  
  LED_init();                           //LED Initialization                 
  BUTTON_init();												//BUTTON Initialization              
	joystick_init();											// Joystick Initialization            
	init_RIT(0x004C4B40);									// RIT Initialization 50 msec       	
	enable_RIT();													// RIT enabled												
	DAC_init();														//DAC Initialization
	
	init_timer(0,0x165A0BC0,0x1DCD6500);	//TIMER0 Initialization for 15s TimerInterval0 and 20s TimerInterval1             
	init_timer(1,0x00BEBC20,0x017D7840);	//TIMER1 Initialization for 0.5s TimerInterval0 and 1s TimerInterval1 
	
	LPC_SC->PCONP |= (1 << 22);						//Activation of power for TIMER2									
	init_timer(2,0x000004ED,0x0);					//TIMER2 Initialization for 50.44us TimerInterval0
	
	conf = 1;															//To enter specific execution in TIMER1_IRQHandler
	enable_timer(0);
	enable_timer(1);
	enable_timer(2);
	
	LPC_SC->PCON |= 0x1;									//power-down	mode										
	LPC_SC->PCON &= 0xFFFFFFFFD;	

	
		
  while (1) {                           //Loop forever and wait for interrupt                      	
		__ASM("wfi");
  }
}