/*      Computer Architectures - 02LSEOV 02LSEOQ - Extrapoint_01				
-----------------------------------------------------------------------    
Author:							Ayman HATOUM - 263893 - Politecnico di Torino           
File name:          IRQ_button.c
Descriptions:       funstions to manage KEY1 and KEY2 interrupts
Creation:						03 January 2019									    
Last Update:  			03 January 2019						               
-----------------------------------------------------------------------	*/

#include "button.h"
#include "lpc17xx.h"
#include "../led/led.h"
#include "../timer/timer.h"

extern int state, i; 

void EINT0_IRQHandler (void)	  	//INT0														 
{
	LPC_SC->EXTINT &= (1 << 0);     //clear pending interrupt         
}
	
void EINT1_IRQHandler (void)	  	//KEY1														 
{
  switch(state)
	{
		case 0:
			reset_timer(0);
			break;
		case 1:
			LED_Out(0x60);							//turn ON LEDS 5 and 6 (green pedestrian and red car)
			reset_timer(0);
			disable_timer(1);
			state = 0;	
			i = 3;											//in case IRQhandler of TIMER1 is not finished yet, so we make sure not to turn OFF LED 5
			break;
		case 2:
			LPC_TIM1->MCR = 0x18;				//activate reset and interrupt of MR1 of TIMER1 and disactivate MR0 completely
			enable_timer(1);
			break;
		default:
			break;
	}
	LPC_SC->EXTINT &= (1 << 1);     //clear pending interrupt
}

void EINT2_IRQHandler (void)	  	//KEY2														 
{
	switch(state)
	{
		case 0:
			reset_timer(0);
			break;
		case 1:
			LED_Out(0x60);							//turn ON LEDS 5 and 6 (green pedestrian and red car)
			reset_timer(0);             
			disable_timer(1);           
			state = 0;                  
			i = 3;                      //in case IRQhandler of TIMER1 is not finished yet, so we make sure not to turn OFF LED 5
			break;                      
		case 2:                       
			LPC_TIM1->MCR = 0x18;       //activate reset and interrupt of MR1 of TIMER1 and disactivate MR0 completely
			enable_timer(1);
			break;
		default:
			break;
	}
  LPC_SC->EXTINT &= (1 << 2);     //clear pending interrupt  
}


