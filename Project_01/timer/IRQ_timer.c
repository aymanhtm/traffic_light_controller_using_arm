/*      Computer Architectures - 02LSEOV 02LSEOQ - Extrapoint_01				
-----------------------------------------------------------------------    
Author:							Ayman HATOUM - 263893 - Politecnico di Torino           
File name:          IRQ_timer.c
Descriptions:       functions to manage T0 and T1 interrupts
Creation:						03 January 2019									    
Last Update:  			03 January 2019						               
-----------------------------------------------------------------------	*/				
		
#include "lpc17xx.h"
#include "timer.h"
#include "../led/led.h"

int state = 0, status = 1, i = 0;

void TIMER0_IRQHandler (void)
{	
	if(state == 0)
	{
		LPC_TIM1->MCR = 3;				//activate reset and interrupt for MR0 of TIMER1
		enable_timer(1);
	}
	else
	{
			status++;
			disable_timer(1);
			reset_timer(1);
			LED_Out(0x88);						//turn ON LEDs 4 and 8 (red pedestrian and green car)
	}
	state++;
	LPC_TIM0->IR = 2;						//clear interrupt flag of MR1
	LPC_TIM0->IR = 1;						//clear interrupt flag of MR0 
	return;
}


void TIMER1_IRQHandler (void)
{
	switch(status)
	{
		case 1:
			if(i%2==0)
				LED_Off(6);						//flashing OFF of LED 5 (green pedestrian)
			else
				LED_On(6);						//flashing ON of LED 5 (green pedestrian)
			i++;
			break;
		case 2:
			LED_Out(0x90);					//turn ON LEDs 4 and 7 (red pedestrian and yellow car)
			state++;
			status++;
			break;
		case 3:
			state = 0;
			LED_Out(0x60);					//turn ON LEDS 5 and 6 (green pedestrian and red car)
			disable_timer(1);
			reset_timer(1);
			enable_timer(0);
			status = 1;
			break;
		default:
			break;
	}
	LPC_TIM1->IR = 2;						//clear interrupt flag of MR1	
  LPC_TIM1->IR = 1;			      //clear interrupt flag of MR0
  return;
}
