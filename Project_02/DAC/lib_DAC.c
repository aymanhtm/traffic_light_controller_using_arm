/*      Computer Architectures - 02LSEOV 02LSEOQ - Extrapoint_02				
-----------------------------------------------------------------------    
Author:							Ayman HATOUM - 263893 - Politecnico di Torino           
File name:          lib_DAC.c
Descriptions:       function to initialize DAC
Creation:						10 January 2019									    
Last Update:  			17 January 2019						               
-----------------------------------------------------------------------	*/

#include "lpc17xx.h"
#include "DAC.h"

void DAC_init (void) 
	{
	LPC_PINCON->PINSEL1 |= (1<<21);
	LPC_PINCON->PINSEL1 &= ~(1<<20);
	LPC_GPIO0->FIODIR |= (1<<26);
	}
	