/*      Computer Architectures - 02LSEOV 02LSEOQ - Extrapoint_02				
-----------------------------------------------------------------------    
Author:							Ayman HATOUM - 263893 - Politecnico di Torino           
File name:          IRQ_adc.c
Descriptions:       functions to manage ADC interrupt
Creation:						10 January 2019									    
Last Update:  			17 January 2019						               
-----------------------------------------------------------------------	*/

#include "lpc17xx.h"
#include "adc.h"
#include "../led/led.h"
#include "../timer/timer.h"

unsigned short AD_current;   
unsigned short AD_last = 0x0;     //Last converted value               
float volume = 5.0;								//start with the higher volume

void ADC_IRQHandler(void) {
  	
  AD_current = ((LPC_ADC->ADGDR>>4) & 0xFFF); 	//Read Conversion Result             
  if(AD_current != AD_last)
	{
		if(AD_current > 0xFAD && AD_current < 0xFFB) 	//for the low volume for levels bigger than 4013 and smaller than 4091
			volume = 5-5*((float)4013/4095);						//for a volume of 0.1
		else
			volume = 5-5*((float)AD_current/4095);			//rest of the resolution we get the float volume factor
		AD_last = AD_current;
  } 
	
}



