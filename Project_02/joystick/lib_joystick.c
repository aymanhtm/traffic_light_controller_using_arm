/*      Computer Architectures - 02LSEOV 02LSEOQ - Extrapoint_02				
-----------------------------------------------------------------------    
Author:							Ayman HATOUM - 263893 - Politecnico di Torino           
File name:          joystick.c
Descriptions:       functions to initialize joystick
Creation:						10 January 2019									    
Last Update:  			17 January 2019						               
-----------------------------------------------------------------------	*/

#include "lpc17xx.h"
#include "joystick.h"

void joystick_init(void) {

  LPC_PINCON->PINSEL3 &= ~(15<<22);	//PIN mode GPIO for left and right sticks
	LPC_GPIO1->FIODIR   &= ~(3<<27);	//P1.27 & P1.28 Input (joysticks on PORT1 defined as Input)
  
}
