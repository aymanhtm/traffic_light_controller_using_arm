/*      Computer Architectures - 02LSEOV 02LSEOQ - Extrapoint_02				
-----------------------------------------------------------------------    
Author:							Ayman HATOUM - 263893 - Politecnico di Torino           
File name:          IRQ_RIT.c
Descriptions:       functions to manage button debouncing routines, joystick, ADC
Creation:						10 January 2019									    
Last Update:  			17 January 2019						               
-----------------------------------------------------------------------	*/	
#include "lpc17xx.h"
#include "RIT.h"
#include "../led/led.h"
#include "../timer/timer.h"
#include "../ADC/adc.h"

extern int conf, state, status, i, t;
int down0 = 0, downkey = 0, maint = 0, int0_pressed = 0;

void RIT_IRQHandler (void)
{					
	static int selectR = 0, selectL =0;

	
	/* button management */
	if(down0!=0)
	{ 
		if((LPC_GPIO2->FIOPIN & (1<<10)) == 0)
		{	/* INT0 pressed */
			down0++;				
			switch(down0)
			{
				case 2:
					LPC_SC->PCONP |= (1 << 22);		//power for TIMER2
					enable_timer(2);							//continues beep untill release
					int0_pressed = 1;							//to enter the usual
					break;
				default:
					break;
			}
		}
		else
		{		/* button released */
			down0 = 0;
			int0_pressed = 0;
				
			disable_timer(2);									//stop beep
			reset_timer(2);
			if(state > 1)											//if state > 1 so TIMER2 is not used then power OFF to save
				LPC_SC->PCONP &= ~(1 << 22);
		}
			
		NVIC_EnableIRQ(EINT0_IRQn);							 //enable Button interrupts			
		LPC_PINCON->PINSEL4    |= (1 << 20);     //External interrupt 0 pin selection 
	}
	
	
	if(downkey != 0)
	{	/* KEY1, KEY2 or INT0 pressed */
		if((LPC_GPIO2->FIOPIN & (1<<11)) == 0 || (LPC_GPIO2->FIOPIN & (1<<12)) == 0 || int0_pressed == 1 )
		{
			downkey++;
			if(downkey == 2)
			switch(state)
				{
					case 0:
						reset_timer(0);
						reset_timer(1);
						reset_timer(2);
						break;
					case 1:
						LED_Out(0x60);				//turn ON LEDS 5 and 6 (green pedestrian and red car)
						LPC_TIM1->MCR = 0x18;	//activate reset and interrupt for MR1 of TIMER1
						reset_timer(0);
						reset_timer(1);
						reset_timer(2);
						conf = 1;
						state = 0;	
						i = 3;											//for debugging, in case IRQhandler of TIMER1 is not finished yet, so we make sure not to turn OFF LED 5
						t = 0;											//start from OFF beeping
						break;
					case 2:
						init_timer(1,0x0,0x07735940);				//activate reset and interrupt of MR1 of TIMER1 and disactivate MR0 completely
						enable_timer(1);
						break;
					default:
						break;
				}
			}
	else
	{
		downkey = 0;
		NVIC_EnableIRQ(EINT1_IRQn);							 // enable Button interrupts			
		LPC_PINCON->PINSEL4    |= (1 << 22);     //External interrupt 0 pin selection 
		NVIC_EnableIRQ(EINT2_IRQn);							 //enable Button interrupts			
		LPC_PINCON->PINSEL4    |= (1 << 24);     //External interrupt 0 pin selection 
	}
}
		
	
	
	
	/* joystick management */
	if((LPC_GPIO1->FIOPIN & (1<<28)) == 0){	// Joytick right pressed 
		selectR++;
		switch(selectR){
			case 1:
				if(state == 0)
				{
					NVIC_DisableIRQ(EINT0_IRQn);		//disable button interrupts
					NVIC_DisableIRQ(EINT1_IRQn);
					NVIC_DisableIRQ(EINT2_IRQn);
					maint = 1;											//active Maint State
					disable_timer(0);
					disable_timer(1);
					disable_timer(2);
					reset_timer(0);
					reset_timer(1);
					reset_timer(2);
					LED_Out(0x90);
					ADC_init();											//Initilization of the ADC
					enable_timer(1);
					enable_timer(2);
				}
				break;
			default:
				break;
		}
	}
	else{
			selectR=0;
	}

	if((LPC_GPIO1->FIOPIN & (1<<27)) == 0){	// Joytick left pressed 
		selectL++;
		switch(selectL){
			case 1:
				if(maint == 1)
				{
					NVIC_EnableIRQ(EINT0_IRQn);			//enable button interrupts again
					NVIC_EnableIRQ(EINT1_IRQn);
					NVIC_EnableIRQ(EINT2_IRQn);
					maint = 0;
					LPC_SC->PCONP &= ~(1<<12);      //Disable power to ADC block for power saving
					/* prepare State 0 */
					enable_timer(0);
					disable_timer(1);
					disable_timer(2);
					reset_timer(1);
					reset_timer(2);
					LED_Out(0x60);
					enable_timer(1);
					enable_timer(2);
				}
				break;
			default:
				break;
		}
	}
	else{
			selectL=0;
	}
	
	
	// ADC management 
	if(maint == 1)
	{
	ADC_start_conversion();		
	}
			
  LPC_RIT->RICTRL |= 0x1;	// clear interrupt flag 
}