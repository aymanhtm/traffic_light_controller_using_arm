/*      Computer Architectures - 02LSEOV 02LSEOQ - Extrapoint_01				
-----------------------------------------------------------------------    
Author:							Ayman HATOUM - 263893 - Politecnico di Torino           
File name:          lib_button.c
Descriptions:       function that initializes Buttons
Creation:						03 January 2019									    
Last Update:  			03 January 2019						               
-----------------------------------------------------------------------	*/
#include "button.h"
#include "lpc17xx.h"

void BUTTON_init(void) {

  LPC_PINCON->PINSEL4    |= (1 << 22);     //External interrupt 0 pin selection 
  LPC_GPIO2->FIODIR      &= ~(1 << 11);    //PORT2.11 defined as input          
  
  LPC_PINCON->PINSEL4    |= (1 << 24);     //External interrupt 0 pin selection 
  LPC_GPIO2->FIODIR      &= ~(1 << 12);    //PORT2.12 defined as input          

  LPC_SC->EXTMODE = 0x7;

  NVIC_EnableIRQ(EINT2_IRQn);              //enable irq in nvic                 
	NVIC_SetPriority(EINT2_IRQn, 1);				 //priority, the lower the better     
  NVIC_EnableIRQ(EINT1_IRQn);              //enable irq in nvic                 
	NVIC_SetPriority(EINT1_IRQn, 1);				 //setting tha same priority of both buttons 

}
