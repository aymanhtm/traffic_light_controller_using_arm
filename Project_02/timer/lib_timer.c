/*      Computer Architectures - 02LSEOV 02LSEOQ - Extrapoint_02				
-----------------------------------------------------------------------    
Author:							Ayman HATOUM - 263893 - Politecnico di Torino           
File name:          lib_timer.c
Descriptions:       functions that manage timers 0, 1 & 2
Creation:						03 January 2019									    
Last Update:  			17 January 2019						               
-----------------------------------------------------------------------	*/

#include "lpc17xx.h"
#include "timer.h"

void enable_timer( uint8_t timer_num )
{
  if ( timer_num == 0 )
		LPC_TIM0->TCR = 1;
  else 
	{
		if (timer_num == 1)
			LPC_TIM1->TCR = 1;
		else
			LPC_TIM2->TCR = 1;								//enables TIMER2
	}
  return;
}

void disable_timer( uint8_t timer_num )
{
  if ( timer_num == 0 )
		LPC_TIM0->TCR = 0;
  else 
	{
		if (timer_num == 1)
			LPC_TIM1->TCR = 0;
		else
			LPC_TIM2->TCR = 0;								//disables TIMER2
	}
  return;
}

void reset_timer( uint8_t timer_num )
{
  uint32_t regVal;

  if ( timer_num == 0 )
  {
	regVal = LPC_TIM0->TCR;
	regVal |= 0x02;
	LPC_TIM0->TCR = regVal;
	regVal &=~(0x02); 									//need to turn off the reset bit
	LPC_TIM0->TCR = regVal;
  }
  else
  {
		if ( timer_num == 1)
		{
			regVal = LPC_TIM1->TCR;
			regVal |= 0x02;
			LPC_TIM1->TCR = regVal;
			regVal &=~(0x02);										//need to turn off the reset bit
			LPC_TIM1->TCR = regVal;
		}
		else																	//resets TIMER2
		{
			regVal = LPC_TIM2->TCR;
			regVal |= 0x02;
			LPC_TIM2->TCR = regVal;
			regVal &=~(0x02);										//need to turn off the reset bit
			LPC_TIM2->TCR = regVal;
		}
  }
  return;
}

uint32_t init_timer ( uint8_t timer_num, uint32_t TimerInterval0, uint32_t TimerInterval1 )
{
  if ( timer_num == 0 )
  {
	LPC_TIM0->MR0 = TimerInterval0;
	LPC_TIM0->MR1 = TimerInterval1;
//*** <<< Use Configuration Wizard in Context Menu >>> ***
// <h> timer0 MCR
//   <e.0> MR0I
//	 <i> 1 Interrupt on MR0: an interrupt is generated when MR0 matches the value in the TC. 0
//	 <i> 0 This interrupt is disabled
//   </e>
//   <e.1> MR0R
//	 <i> 1 Reset on MR0: the TC will be reset if MR0 matches it.
//	 <i> 0 Feature disabled.
//   </e>
//   <e.2> MR0S
//	 <i> 1 Stop on MR0: the TC and PC will be stopped and TCR[0] will be set to 0 if MR0 matches the TC
//	 <i> 0 Feature disabled.
//   </e>
//   <e.3> MR1I
//	 <i> 1 Interrupt on MR1: an interrupt is generated when MR0 matches the value in the TC. 0
//	 <i> 0 This interrupt is disabled
//   </e>
//   <e.4> MR1R
//	 <i> 1 Reset on MR1: the TC will be reset if MR0 matches it.
//	 <i> 0 Feature disabled.
//   </e>
//   <e.5> MR1S
//	 <i> 1 Stop on MR1: the TC and PC will be stopped and TCR[1] will be set to 0 if MR1 matches the TC
//	 <i> 0 Feature disabled.
//   </e>
//   <e.6> MR2I
//	 <i> 1 Interrupt on MR2: an interrupt is generated when MR2 matches the value in the TC.
//	 <i> 0 This interrupt is disabled
//   </e>
//   <e.7> MR2R
//	 <i> 1 Reset on MR2: the TC will be reset if MR2 matches it.
//	 <i> 0 Feature disabled.
//   </e>
//   <e.8> MR2S
//	 <i> 1 Stop on MR2: the TC and PC will be stopped and TCR[2] will be set to 0 if MR2 matches the TC
//	 <i> 0 Feature disabled.
//   </e>
//   <e.9> MR3I
//	 <i> 1 Interrupt on MR3: an interrupt is generated when MR3 matches the value in the TC.
//	 <i> 0 This interrupt is disabled
//   </e>
//   <e.10> MR3R
//	 <i> 1 Reset on MR3: the TC will be reset if MR3 matches it.
//	 <i> 0 Feature disabled.
//   </e>
//   <e.11> MR3S
//	 <i> 1 Stop on MR3: the TC and PC will be stopped and TCR[3] will be set to 0 if MR3 matches the TC
//	 <i> 0 Feature disabled.
//   </e>
	LPC_TIM0->MCR = 0x39;								//activate interrupt for MR0 AND stop, reset and interrupt for MR1
// </h>
//*** <<< end of configuration section >>>    ***
	NVIC_SetPriority(TIMER0_IRQn, 2);		//equal priority as TIMER1 but lower than KEY1 and KEY2
	NVIC_EnableIRQ(TIMER0_IRQn);
	return (1);
  }
  else 
	{
		if ( timer_num == 1 )
  {
	LPC_TIM1->MR0 = TimerInterval0;
	LPC_TIM1->MR1 = TimerInterval1;
	LPC_TIM1->MCR = 0x18;									//activate reset and interrupt for MR1 of TIMER1

	NVIC_SetPriority(TIMER1_IRQn, 2);			//equal priority as TIMER0 but lower than SWITCHES
	NVIC_EnableIRQ(TIMER1_IRQn);
	return (1);
  }
	else																	//Initializes TIMER2	
	{
		LPC_TIM2->MR0 = TimerInterval0;
		LPC_TIM2->MR1 = TimerInterval1;
		LPC_TIM2->MCR = 3;									//activate reset and interrupt for MR0 of TIMER2

		NVIC_SetPriority(TIMER2_IRQn, 2);		//equal priority as TIMER0 and TIMER1 but lower than SWITCHES
		NVIC_EnableIRQ(TIMER2_IRQn);
		return (1);
	}
}
  return (0);
}