/*      Computer Architectures - 02LSEOV 02LSEOQ - Extrapoint_02				
-----------------------------------------------------------------------    
Author:							Ayman HATOUM - 263893 - Politecnico di Torino           
File name:          lib_led.c
Descriptions:       function that initializes LEDs and switch LED 5 and 6 ON
Creation:						03 January 2019									    
Last Update:  			03 January 2019						               
-----------------------------------------------------------------------	*/

#include "lpc17xx.h"
#include "led.h"

void LED_init(void) {

  LPC_PINCON->PINSEL4 &= 0xFFFF0000;	//PIN mode GPIO (00b value per P2.0 to P2.7)
	LPC_GPIO2->FIODIR   |= 0x000000FF;  //P2.0...P2.7 Output (LEDs on PORT2 defined as Output)
  LPC_GPIO2->FIOSET    = 0x00000060;	//turn ON LEDS 5 and 6 (green pedestrian and red car)
	
}