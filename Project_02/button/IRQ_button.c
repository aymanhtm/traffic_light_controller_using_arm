/*      Computer Architectures - 02LSEOV 02LSEOQ - Extrapoint_02				
-----------------------------------------------------------------------    
Author:							Ayman HATOUM - 263893 - Politecnico di Torino           
File name:          IRQ_button.c
Descriptions:       funstions to manage KEY1, KEY2 & INT0 interrupts
Creation:						10 January 2019									    
Last Update:  			17 January 2019						               
-----------------------------------------------------------------------	*/

#include "button.h"
#include "lpc17xx.h"
#include "../timer/timer.h"
#include "../RIT/RIT.h"

extern int state, i, down0, downkey; 

void EINT0_IRQHandler (void)	  			//INT0														 
{
	NVIC_DisableIRQ(EINT0_IRQn);
	LPC_PINCON->PINSEL4 &= ~(1 << 20);	//Select GPIO in order to read it
	down0 = 1; 													//To enter its specific execution in the RIT_IRQHandler
	downkey = 1;												//To enter the usual button execution in the RIT_IRQHandler
	reset_RIT();												//To ensure the debouncing at 50ms
	
	LPC_SC->EXTINT &= (1 << 0);     		//clear pending interrupt         
}
	
void EINT1_IRQHandler (void)	  			//KEY1														 
{
	NVIC_DisableIRQ(EINT1_IRQn);
	LPC_PINCON->PINSEL4 &= ~(1 << 22);	//Select GPIO in order to read it
	NVIC_DisableIRQ(EINT2_IRQn);
	LPC_PINCON->PINSEL4 &= ~(1 << 24);	//Select GPIO in order to read it
	downkey = 1; 												//To enter the usual button execution in the RIT_IRQHandler
	reset_RIT();
 
	LPC_SC->EXTINT &= (1 << 1);     		//clear pending interrupt
}

void EINT2_IRQHandler (void)	  			//KEY2														 
{
	NVIC_DisableIRQ(EINT2_IRQn);
	LPC_PINCON->PINSEL4 &= ~(1 << 24);	//Select GPIO in order to read it
	NVIC_DisableIRQ(EINT1_IRQn);
	LPC_PINCON->PINSEL4 &= ~(1 << 22);	//Select GPIO in order to read it
	downkey = 1; 												//To enter the usual button execution in the RIT_IRQHandler
	reset_RIT();
	
  LPC_SC->EXTINT &= (1 << 2);    			//clear pending interrupt  
}


